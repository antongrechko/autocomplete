import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * Created by Anton Grechko on 09.06.2015.
 */
public class AutocompleteTest {

    private static final String TEST_INPUT_1 = "\\src\\main\\resources\\test.in";

    private static final String TEST_INPUT_2 = "\\src\\main\\resources\\test2.in";

    /**
     * Expect program to last less than 10 seconds
     */
    @Test(timeout = 10 * 1000)
    public void AutocompleteTimeConsumptionTest() {

        try {
            InputStream in = new FileInputStream(new File(new File("").getAbsolutePath() + TEST_INPUT_1));
            Autocomplete program = new Autocomplete();
            program.start(in, new FileOutputStream(FileDescriptor.out));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    /**
     * Expect program to work correct on test data from example
     */
    @Test
    public void correctSuggestionsOrderTest() {

        String correctResult = "kanojo\n"
                + "kare\n"
                + "korosu\n"
                + "karetachi\n"
                + "\n"
                + "kanojo\n"
                + "kare\n"
                + "karetachi\n"
                + "\n"
                + "kare\n"
                + "karetachi\n"
                + "\n";

        Autocomplete program = new Autocomplete();

        try {
            InputStream in = new FileInputStream(new File(new File("").getAbsolutePath() + TEST_INPUT_2));

            ByteArrayOutputStream baos = new ByteArrayOutputStream(100);
            BufferedOutputStream out = new BufferedOutputStream(baos);

            program.start(in, out);

            Scanner scanner =
                    new Scanner(new ByteArrayInputStream(baos.toByteArray()), StandardCharsets.US_ASCII.name());
            StringBuffer sb = new StringBuffer();
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine() + '\n');
            }

            Assert.assertEquals(correctResult, sb.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

}
