import java.io.FileDescriptor;
import java.io.FileOutputStream;

/**
 * Created by Anton Grechko on 07.06.2015.
 */
public class Main {

    public static void main(String[] args) {

        Autocomplete program = new Autocomplete();
        program.start(System.in, new FileOutputStream(FileDescriptor.out));
    }
}
