import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * Created by Anton Grechko on 07.06.2015.
 */
public class Autocomplete {

    /**
     * Encoding
     */
    private static final String ASCII_ENCODING = StandardCharsets.US_ASCII.name();

    /**
     * Show only first ten autocomplete suggestions
     */
    private static final int MAX_SUGGESTIONS = 10;

    /**
     * Ternary search dictionary data structure
     * Modified to display the words in alphabetical order if weight of neighbor nodes are equal
     */
    private SuggestTree dictionary;

    /**
     * Start program
     *
     * @param in - data input stream
     * @param out - suggestions output stream
     */
    public void start(InputStream in, OutputStream out) {

        Scanner input = new Scanner(in, ASCII_ENCODING);
        dictionary = new SuggestTree(MAX_SUGGESTIONS);
        fillSuggestTree(input);

        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out), 512);

            int suggestionsLines = input.nextInt();
            while (suggestionsLines-- > 0) {
                String[] suggestions = findAutocompleteSuggestions(input.next());
                if (suggestions != null) {
                    for (String sugg : suggestions) {
                        writer.write(sugg + '\n');
                    }
                }
                writer.write('\n');
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fillSuggestTree(Scanner input) {

        int wordsInDictionary = input.nextInt();
        while (input.hasNextLine()) {
            if (wordsInDictionary-- > 0) {
                dictionary.put(input.next(), input.nextInt());
            } else {
                break;
            }
        }
    }

    private String[] findAutocompleteSuggestions(String sugg) {

        String[] result = null;
        SuggestTree.Node node = dictionary.getAutocompleteSuggestions(sugg);

        if (node != null) {
            result = new String[node.listLength()];
            for (int i = 0; i < node.listLength(); i++) {
                result[i] = node.getSuggestion(i).getTerm();
            }
            return result;
        }

        return result;
    }

}
